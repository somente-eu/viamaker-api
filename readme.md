# Desafio Viamaker

Desenvolvido por Adrian Prado em resposta ao [Desafio Viamaker](https://github.com/vitorric/viamaker-desafio-nodejs).

## Tecnologias e Libs Usadas

- Node.js (12.14.1)
- TypeScript (2.9.2)
- Express.js (4.16.4)
- MongoDB
- Swagger

## Preparando a Aplicação

### 1. Instalar Dependências

Após clonar este repositório, instalar as dependências (preferencialmente usando o yarn) com o comando:

`yarn install`

### 2. Fazer a compilação do JavaScript:

`yarn build`

### 3. Rodar a aplicação:
 
`yarn start`

Então, no console, a saída deverá ser algo parecido com isso:

    yarn run v1.17.3
    $ node dist/index.js
    [ 'Currently Server Version: v1.0.0' ]
    [ 'Database Connected.' ]
    [ 'API is ready. Listening on port 9000' ]
    [ 'API Started at 2021-02-17T16:37:13-03:00' ]

## Documentação

Para ter acesso à documentação, com a aplicação rodando, basta acessar o endereço no navegador:

`http://localhost:9000/api-docs`

**A documentação foi toda redigida na língua Inglesa.*

## Testes

Foram escritos alguns testes unitários utilizando libs como `mocha` e `chai` para sua construção. Eles podem ser executados com o comando:

`yarn test`

