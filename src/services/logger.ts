const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const files = new transports.File({ filename: 'api.log' });

const myFormat = printf((info: any) => {
    return `${info.timestamp} [${info.level}]: ${info.message}`;
});

const logger = createLogger({
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [
        files
    ]
});

export const logError = function (...msg: any[]) {
    let err = new Error(msg[0]);
    logger.error(`${msg} - ${err.stack}`);
    console.error(msg);
};

export const logInfo = function (...msg: any[]) {
    logger.info(`${msg}`);
    console.log(msg);
};

export const logRequest = (req: any, res: any, next: any) => {
    try {
        const { httpVersion, url, method, baseUrl, originalUrl, query, body } = req;
        logInfo(`[HTTP ${httpVersion} => ${url} (${method}) = ${originalUrl} (${baseUrl}) :: ${JSON.stringify(query)} :: ${JSON.stringify(body)}]`);
    }
    catch (err) {
        logError(err);
    }
    next();
};