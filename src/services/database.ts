import { DB_NAME, DB_URI } from '../settings';
import { logInfo } from './logger';
const mongodb = require('mongodb');
const ObjectId = mongodb.ObjectId;

class Database {
    static connected: boolean = false;
    static client: any = null;
    static db: any = null;
    static collections: {
        schools?: any,
        groups?: any,
        students?: any,
    } = {};

    static oid = ObjectId;

    static async create() {
        return await new Promise<any>(async (resolve, reject) => {
            try {
                const uri = DB_URI;
                let client = new mongodb.MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
                await client.connect((err: any) => {
                    if (!err) {
                        this.connected = true;

                        client.on('connectionClosed', () => {
                            logInfo('Database Connection closed.');
                        });

                        this.db = client.db(DB_NAME);

                        /**
                         * a function to startup collections
                         * @param collectionName is the name of the collection to setup
                         */
                        const setupCollection = (collectionName: string) => {
                            (this.collections as any)[collectionName] = this.db.collection(collectionName);
                        };

                        const collections = [
                            'schools',
                            'groups',
                            'students',
                        ];

                        collections.map(c => setupCollection(c));

                        logInfo('Database Connected.');
                        resolve(true);
                    } else {
                        console.log(err, process.env.DB_URL);
                    }
                });

                this.client = client;
            } catch (err) {
                reject('Database Connection failed.');
            }
        });
    }

    static close() {
        this.client.close();
    }
}

export default Database;