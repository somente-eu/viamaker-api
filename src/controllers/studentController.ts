import AppError from '../components/error';
import { Request, Response } from 'express';
import * as Yup from 'yup';
import { getValidatedObjectFromSchema, validateDataSchema } from '../helpers/validators';
import StudentsCollection from '../database/collections/students';

export default class StudentController {
    static async create(req: Request, res: Response) {
        let { body } = req;

        const schema = Yup.object().shape({
            name: Yup.string().required('NO_STUDENT_NAME'),
            groupId: Yup.string().required('NO_GROUP_ID'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            let created = await StudentsCollection.create(obj.name, obj.groupId);
            return created;
        }

        throw new AppError(AppError.errorModels.UKNOWN_STUDENT_ACTION);
    }

    static async read(req: Request, res: Response) {
        const { _id } = req.params;
        const body = { _id };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_STUDENT'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            let school = await StudentsCollection.findById(obj._id);
            return school;
        }

        throw new AppError(AppError.errorModels.UKNOWN_STUDENT_ACTION);
    }

    static async list(req: Request, res: Response) {
        let students = await StudentsCollection.findAll();
        return students;
    }

    static async update(req: Request, res: Response) {
        const { _id } = req.params;
        const body = {
            ...req.body,
            _id,
        };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_STUDENT'),
            name: Yup.string().required('NO_STUDENT_NAME'),
            groupId: Yup.string().required('NO_GROUP_ID'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            const updated = await StudentsCollection.update(obj._id, obj);
            return updated;
        }

        throw new AppError(AppError.errorModels.UKNOWN_STUDENT_ACTION);
    }

    static async delete(req: Request, res: Response) {
        const { _id } = req.params;
        const body = { _id };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_STUDENT'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);
            return await StudentsCollection.remove(obj._id);
        }

        throw new AppError(AppError.errorModels.UKNOWN_STUDENT_ACTION);
    }
}