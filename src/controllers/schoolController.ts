import AppError from '../components/error';
import { Request, Response } from 'express';
import * as Yup from 'yup';
import { getValidatedObjectFromSchema, validateDataSchema } from '../helpers/validators';
import SchoolsCollection from '../database/collections/schools';

export default class SchoolController {
    static async create(req: Request, res: Response) {
        let { body } = req;

        const schema = Yup.object().shape({
            name: Yup.string().required('NO_SCHOOL_NAME'),
            cnpj: Yup.string().required('NO_SCHOOL_IDENTIFICATION').min(14, 'INVALID_SCHOOL_IDENTIFICATION'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            let created = await SchoolsCollection.create(obj.name, obj.cnpj);
            return created;
        }

        throw new AppError(AppError.errorModels.UKNOWN_SCHOOL_ACTION);
    }

    static async read(req: Request, res: Response) {
        const { _id } = req.params;
        const body = { _id };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_SCHOOL'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            let school = await SchoolsCollection.findById(obj._id);
            return school;
        }

        throw new AppError(AppError.errorModels.UKNOWN_SCHOOL_ACTION);
    }

    static async list(req: Request, res: Response) {
        let schools = await SchoolsCollection.findAll();
        return schools;
    }

    static async update(req: Request, res: Response) {
        const { _id } = req.params;
        const body = {
            ...req.body,
            _id,
        };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_SCHOOL'),
            name: Yup.string().required('NO_SCHOOL_NAME'),
            cnpj: Yup.string().required('NO_SCHOOL_IDENTIFICATION').min(14, 'INVALID_SCHOOL_IDENTIFICATION'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            const updated = await SchoolsCollection.update(obj._id, obj);
            return updated;
        }

        throw new AppError(AppError.errorModels.UKNOWN_SCHOOL_ACTION);
    }

    static async delete(req: Request, res: Response) {
        const { _id } = req.params;
        const body = { _id };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_SCHOOL'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);
            return await SchoolsCollection.remove(obj._id);
        }

        throw new AppError(AppError.errorModels.UKNOWN_SCHOOL_ACTION);
    }
}