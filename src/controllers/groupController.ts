import AppError from '../components/error';
import { Request, Response } from 'express';
import * as Yup from 'yup';
import { getValidatedObjectFromSchema, validateDataSchema } from '../helpers/validators';
import GroupsCollection from '../database/collections/groups';

export default class GroupController {
    static async create(req: Request, res: Response) {
        let { body } = req;

        const schema = Yup.object().shape({
            name: Yup.string().required('NO_GROUP_NAME'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            let created = await GroupsCollection.create(obj.name);
            return created;
        }

        throw new AppError(AppError.errorModels.UKNOWN_GROUP_ACTION);
    }

    static async read(req: Request, res: Response) {
        const { _id } = req.params;
        const body = { _id };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_GROUP'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            let school = await GroupsCollection.findById(obj._id);
            return school;
        }

        throw new AppError(AppError.errorModels.UKNOWN_GROUP_ACTION);
    }

    static async list(req: Request, res: Response) {
        let groups = await GroupsCollection.findAll();
        return groups;
    }

    static async update(req: Request, res: Response) {
        const { _id } = req.params;
        const body = {
            ...req.body,
            _id,
        };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_GROUP'),
            name: Yup.string().required('NO_GROUP_NAME'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);

            const updated = await GroupsCollection.update(obj._id, obj);
            return updated;
        }

        throw new AppError(AppError.errorModels.UKNOWN_GROUP_ACTION);
    }

    static async delete(req: Request, res: Response) {
        const { _id } = req.params;
        const body = { _id };
        const schema = Yup.object().shape({
            _id: Yup.string().required('INVALID_GROUP'),
        });

        if (await validateDataSchema(body, schema, res)) {
            let obj = await getValidatedObjectFromSchema(schema, body);
            return await GroupsCollection.remove(obj._id);
        }

        throw new AppError(AppError.errorModels.UKNOWN_GROUP_ACTION);
    }
}