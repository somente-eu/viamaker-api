export const overrideDefaults = () => {
    // JavaScript
    // Array
    Array.prototype.mapAsync = async function <U>(callbackfn: (value: any, index: number, arr: any[]) => U) {
        for (let i = 0; i < this.length; i++) {
            await callbackfn(this[i], i, this);
        }
        return this;
    };
};