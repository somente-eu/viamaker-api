import { Response } from 'express';
import AppError from '../components/error';
import { logError } from '../services/logger';

export async function validateDataSchema(body: any, schema: any, res: Response,) {
    try {
        let valid = await schema.strict().validate(body);
        if (!(valid)) {
            res.json({ success: false, message: 'INVALID_DATA' });
            return false;
        }
        return true;
    } catch (err) {
        logError(err.message);
        let apperr = new AppError(AppError.errorModels.VALIDATE_FAIL);
        apperr.message = err.message;
        throw apperr;
    }
}

export async function getValidatedObjectFromSchema(schema: any, body: any) {
    try {
        if (Array.isArray(body)) {
            let objs: any[] = [];

            if (schema._subType.type === "object") {
                for (let i = 0; i < body.length; i++) {
                    let currObj = body[i];
                    let validObj = await getValidatedObjectFromSchema(schema._subType, currObj);
                    objs.push(validObj);
                }
            } else
                objs = body;
            return objs;
        }
        if (schema && schema.fields) {
            let fields = Object.keys(schema.fields);
            let obj: any = Object.assign({});
            await fields.mapAsync(async (field) => {
                if (typeof body[field] === 'object')
                    obj[field] = await getValidatedObjectFromSchema(schema.fields[field], body[field]);
                else
                    obj[field] = body[field];
            });

            return obj;
        }

        return body;
    } catch (err) {
        console.log(err);
    }
}

export async function validateObjSchema(obj: any, schema: any) {
    try {
        let valid = await schema.strict().validate(obj);
        if (!(valid)) {
            throw new AppError(AppError.errorModels.INVALID_OBJ_SCHEMA);
        }
        return true;
    } catch (err) {
        logError(err.message);
        let apperr = new AppError(AppError.errorModels.VALIDATE_FAIL);
        apperr.message = err.message;
        throw apperr;
    }
}