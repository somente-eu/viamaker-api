declare interface Array<T> {
    mapAsync<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): Promise<any>;
}

declare module "*.json" {
    const value: any;
    export default value;
}