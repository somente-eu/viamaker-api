declare namespace Express {
  interface Request {
    /* Token of the user sent with the request. */
    token: string;
  }
}