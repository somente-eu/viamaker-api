import { IStudent } from "../../interfaces";
import Database from "../../services/database";
import AppError from '../../components/error';
import * as moment from 'moment';

export default class StudentsCollection {
    static async create(name: string, groupId: string): Promise<IStudent> {
        const { students } = Database.collections;
        let student: IStudent = {
            name,
            groupId
        };

        let insert = await students.insertOne(student);
        student._id = insert.insertedId;
        return student;
    }

    static async findAll(): Promise<IStudent[]> {
        const { students } = Database.collections;
        let results = await students.find().toArray();
        if (results.length)
            return results;
        return [];
    }

    static async findById(_id: string): Promise<IStudent> {
        const { students } = Database.collections;
        let results = await students.find({
            _id: Database.oid(_id)
        }).toArray();
        if (results.length)
            return results[0];
        throw new AppError(AppError.errorModels.UKNOWN_STUDENT_ACTION);
    }

    static async update(_id: string, updates: any): Promise<boolean> {
        const { students } = Database.collections;
        delete updates._id;
        let updated = await students.updateOne({
            _id: Database.oid(_id)
        }, {
            $set: {
                ...updates,
                updated: moment().toDate()
            }
        });

        return updated.result.ok ? true : false;
    }

    static async remove(_id: string): Promise<boolean> {
        const { students } = Database.collections;

        let removed = await students.deleteOne({
            _id: Database.oid(_id),
        });

        return removed.result.ok ? true : false;
    }
}