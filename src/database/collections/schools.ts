import { ISchool } from "../../interfaces";
import Database from "../../services/database";
import AppError from '../../components/error';
import * as moment from 'moment';

export default class SchoolsCollection {
    static async create(name: string, cnpj: string): Promise<ISchool> {
        const { schools } = Database.collections;
        let school: ISchool = {
            name,
            cnpj,
        };

        let insert = await schools.insertOne(school);
        school._id = insert.insertedId;
        return school;
    }

    static async findAll(): Promise<ISchool[]> {
        const { schools } = Database.collections;
        let results = await schools.find().toArray();
        if (results.length)
            return results;
        return [];
    }

    static async findById(_id: string): Promise<ISchool> {
        const { schools } = Database.collections;
        let results = await schools.find({
            _id: Database.oid(_id)
        }).toArray();
        if (results.length)
            return results[0];
        throw new AppError(AppError.errorModels.UKNOWN_SCHOOL_ACTION);
    }

    static async update(_id: string, updates: any): Promise<boolean> {
        const { schools } = Database.collections;
        delete updates._id;
        let updated = await schools.updateOne({
            _id: Database.oid(_id)
        }, {
            $set: {
                ...updates,
                updated: moment().toDate()
            }
        });

        return updated.result.ok ? true : false;
    }

    static async remove(_id: string): Promise<boolean> {
        const { schools } = Database.collections;

        let removed = await schools.deleteOne({
            _id: Database.oid(_id),
        });

        return removed.result.ok ? true : false;
    }
}