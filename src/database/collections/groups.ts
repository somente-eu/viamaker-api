import { IGroup } from "../../interfaces";
import Database from "../../services/database";
import AppError from '../../components/error';
import * as moment from 'moment';

export default class GroupsCollection {
    static async create(name: string): Promise<IGroup> {
        const { groups } = Database.collections;
        let group: IGroup = {
            name,
        };

        let insert = await groups.insertOne(group);
        group._id = insert.insertedId;
        return group;
    }

    static async findAll(): Promise<IGroup[]> {
        const { groups } = Database.collections;
        let results = await groups.find().toArray();
        if (results.length)
            return results;
        return [];
    }

    static async findById(_id: string): Promise<IGroup> {
        const { groups } = Database.collections;
        let results = await groups.find({
            _id: Database.oid(_id)
        }).toArray();
        if (results.length)
            return results[0];
        throw new AppError(AppError.errorModels.UKNOWN_GROUP_ACTION);
    }

    static async update(_id: string, updates: any): Promise<boolean> {
        const { groups } = Database.collections;
        delete updates._id;
        let updated = await groups.updateOne({
            _id: Database.oid(_id)
        }, {
            $set: {
                ...updates,
                updated: moment().toDate()
            }
        });

        return updated.result.ok ? true : false;
    }

    static async remove(_id: string): Promise<boolean> {
        const { groups } = Database.collections;

        let removed = await groups.deleteOne({
            _id: Database.oid(_id),
        });

        return removed.result.ok ? true : false;
    }
}