import { setExternalRoute } from './';
import ControllerConstructor from '../controllers/';
import SchoolController from '../controllers/schoolController';
import GroupController from '../controllers/groupController';
import StudentController from '../controllers/studentController';

const fn = (fn: any) => {
    return (req: any, res: any) => {
        ControllerConstructor.RouteConstructor(req, res, fn);
    };
};

export function setRoutesUp() {
    setExternalRoute('get', 'schools', fn(SchoolController.list));
    setExternalRoute('post', 'schools', fn(SchoolController.create));
    setExternalRoute('get', 'schools/:_id', fn(SchoolController.read));
    setExternalRoute('put', 'schools/:_id', fn(SchoolController.update));
    setExternalRoute('delete', 'schools/:_id', fn(SchoolController.delete));

    setExternalRoute('get', 'groups', fn(GroupController.list));
    setExternalRoute('post', 'groups', fn(GroupController.create));
    setExternalRoute('get', 'groups/:_id', fn(GroupController.read));
    setExternalRoute('put', 'groups/:_id', fn(GroupController.update));
    setExternalRoute('delete', 'groups/:_id', fn(GroupController.delete));

    setExternalRoute('get', 'students', fn(StudentController.list));
    setExternalRoute('post', 'students', fn(StudentController.create));
    setExternalRoute('get', 'students/:_id', fn(StudentController.read));
    setExternalRoute('put', 'students/:_id', fn(StudentController.update));
    setExternalRoute('delete', 'students/:_id', fn(StudentController.delete));
}