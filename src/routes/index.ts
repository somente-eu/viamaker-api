import { setRoutesUp } from './routes';
import App from '../app';

export default function setupRouter(): any {
    setRoutesUp();

    /**
     * static routes
     */
    App.instance.get('/info', (req: any, res: any) => {
        let obj = {
            cwd: process.cwd(),
            env: process.env,
        };

        return res.json(obj);
    });
}

export function setExternalRoute(method: string, route: string, func: any) {
    const api_version = 'v1';
    const app = App.instance;
    let registerRoute: any = app.get.bind(app);

    switch (method) {
        case "post": { registerRoute = app.post.bind(app); break; }
        case "put": { registerRoute = app.put.bind(app); break; }
        case "delete": { registerRoute = app.delete.bind(app); break; }
    }
    registerRoute(`/${api_version}/${route}`, func);
}