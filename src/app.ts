import { logInfo, logRequest } from './services/logger';
import * as moment from 'moment';

// Routes
import route from './routes/';
import Database from './services/database';
import { overrideDefaults } from './helpers/utils';

const pkg = require('../package.json');
const path = require('path');

// Libs
const cors = require('cors');
const bodyParser = require('body-parser');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load(path.join(__dirname, '../docs/swagger.yaml'));

export default class App {
    static fullyLoaded = false;

    static express = require('express');
    static instance = App.express();

    configureApp(app: any) {
        app.use(bodyParser.json({
            limit: 4194304
        }));
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(cors());
        app.use(logRequest);

        route();

        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    }

    async prepareDatabase() {
        await Database.create();
    }

    async start() {
        logInfo(`Currently Server Version: v${pkg.version}`);

        overrideDefaults();
        await this.prepareDatabase();
        this.configureApp(App.instance);

        App.instance.listen(process.env.HTTP_PORT);
        logInfo(`API is ready. Listening on port ${process.env.HTTP_PORT}`);

        let startMsg = `API Started at ${moment().format()}`;
        logInfo(startMsg);
    }
}