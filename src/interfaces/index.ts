
/**
 * School entity
 */
export interface ISchool {
    _id?: string;
    /**
     * school name
     */
    name: string;
    /**
     * school identification code
     */
    cnpj: string;
    /**
     * school update datetime
     */
    updated?: Date;
}

/**
 * Group entity
 */
export interface IGroup {
    _id?: string;
    /**
     * group name
     */
    name: string;
    /**
     * group update datetime
     */
    updated?: Date;
}

/**
 * Student entity
 */
export interface IStudent {
    _id?: string;
    /**
     * student name
     */
    name: string;
    /**
     * student group
     */
    groupId: string;
    /**
     * student update datetime
     */
    updated?: Date;
}