const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../.env') });

export const DB_URI = process.env.DB_URI;
export const DB_NAME = process.env.DB_NAME;