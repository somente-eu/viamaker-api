import App from "../app";
import SchoolController from "../controllers/schoolController";
import { overrideDefaults } from "../helpers/utils";

overrideDefaults();

const chai = require('chai');
const http = require('chai-http');
const subSet = require('chai-subset');

chai.use(http);
chai.use(subSet);

const app = new App();

describe('Starting app...', () => {
    let lastCreatedId = '';

    it('should start app', async () => {
        await app.start();
    })
});

describe('Test schools routes', () => {

    let lastCreatedId = '';

    it('should create a school with no errors', (done) => {
        chai.request(App.instance)
            .post(`/v1/schools`)
            .send({
                name: 'Nova Escola',
                cnpj: '11000011000111'
            })
            .end((err: any, res: any) => {
                const body = res.body;
                const result = body.result;

                if (body.success)
                    lastCreatedId = result._id;

                chai.expect(err).to.be.null;
                chai.expect(res).to.have.status(200);
                chai.expect(body).to.have.property('success', true);
                chai.expect(body).not.to.have.property('code');

                done();
            });
    });

    it('should return a list of schools with no errors', (done) => {
        chai.request(App.instance)
            .get(`/v1/schools`)
            .end((err: any, res: any) => {
                const body = res.body;
                const result = body.result;

                chai.expect(err).to.be.null;
                chai.expect(res).to.have.status(200);
                chai.expect(body).to.have.property('success', true);
                chai.expect(body).not.to.have.property('code');
                chai.expect(result).to.be.instanceOf(Array);

                done();
            });
    });

    it('should update a school with no errors', (done) => {
        chai.request(App.instance)
            .put(`/v1/schools/${lastCreatedId}`)
            .send({
                name: 'Velha Escola',
                cnpj: '11000011000112'
            })
            .end((err: any, res: any) => {
                const body = res.body;
                const result = body.result;

                chai.expect(err).to.be.null;
                chai.expect(res).to.have.status(200);
                chai.expect(body).to.have.property('success', true);
                chai.expect(body).not.to.have.property('code');
                chai.expect(result).to.be.equal(true);

                done();
            });
    });

    it('should return info of a school with no errors', (done) => {
        chai.request(App.instance)
            .get(`/v1/schools/${lastCreatedId}`)
            .end((err: any, res: any) => {
                const body = res.body;
                const result = body.result;

                chai.expect(err).to.be.null;
                chai.expect(res).to.have.status(200);
                chai.expect(body).to.have.property('success', true);
                chai.expect(body).not.to.have.property('code');
                chai.expect(result).to.be.an.instanceOf(Object);

                done();
            });
    });

    it('should delete a school with no errors', (done) => {
        chai.request(App.instance)
            .delete(`/v1/schools/${lastCreatedId}`)
            .end((err: any, res: any) => {
                const body = res.body;
                const result = body.result;

                chai.expect(err).to.be.null;
                chai.expect(res).to.have.status(200);
                chai.expect(body).to.have.property('success', true);
                chai.expect(body).not.to.have.property('code');
                chai.expect(result).to.be.equal(true);

                done();
            });
    });
});