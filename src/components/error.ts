interface IErrorModel {
    code: number;
    message: string;
}

export default class AppError implements Error {
    static errorModels = {
        VALIDATE_FAIL: 1000,
        INVALID_OBJ_SCHEMA: 1001,
        UKNOWN_SCHOOL_ACTION: 1002,
        UKNOWN_GROUP_ACTION: 1003,
        UKNOWN_STUDENT_ACTION: 1004,
    };

    name: string = "";
    message: string;
    code: number;

    constructor(errcode: number) {
        let msg = "UNKNOWN";

        let keys = Object.keys(AppError.errorModels);
        keys.map((k) => {
            if ((AppError.errorModels as any)[k] === errcode)
                msg = k;
        });

        this.code = errcode;
        this.message = msg;
    }
}