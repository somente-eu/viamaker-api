swagger: "2.0"
info:
  version: 1.0.0
  title: Desafio Viamaker API
  description: A simple API to serve a Viamaker test (at [viamaker-desafio-nodejs](https://github.com/vitorric/viamaker-desafio-nodejs)).
  contact:
    email: "adrianprado@live.com"

schemes:
  - http
host: localhost:9000
basePath: /v1

tags:
- name: "schools"
  description: "Access and update Schools data"
- name: "groups"
  description: "Manage and access Groups data"
- name: "students"
  description: "Access and manage Students data"

definitions:
  SchoolCreateAndUpdateBody:
    type: "object"
    properties:
      name:
        type: "string"
        default: "Nova Escola"
      cnpj:
        type: "string"
        default: "83.021.832/0001-11"
  School:
    type: "object"
    properties:
      _id:
        type: "string"
      name:
        type: "string"
      cnpj:
        type: "string"
      updated:
        type: "string"
        format: "date-time"

  GroupCreateAndUpdateBody:
    type: "object"
    properties:
      name:
        type: "string"
        default: "Nova Turma"
  Group:
    type: "object"
    properties:
      _id:
        type: "string"
      name:
        type: "string"
      updated:
        type: "string"
        format: "date-time"

  StudentCreateAndUpdateBody:
    type: "object"
    properties:
      name:
        type: "string"
        default: "Novo Aluno"
      groupId:
        type: "string"
        default: "602c8c3fcc7040ceffe8ea8c"
  Student:
    type: "object"
    properties:
      _id:
        type: "string"
      name:
        type: "string"
      groupId:
        type: "string"
      updated:
        type: "string"
        format: "date-time"



paths:
  /schools:
    get:
      tags:
        - schools
      summary: Get all schools
      description: Returns a list containing all schools.
      responses:
        200:
          description: Returns request's success and a list of Schools
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              result:
                type: "array"
                items:
                  $ref: "#/definitions/School"
    
    post:
      tags:
        - schools
      summary: Insert/create new school
      description: Creates a new school in the database.
      parameters:
      - in: "body"
        name: "body"
        description: "Object containing the infos needed to be added in the database.\n Property `cnpj` must have at least 14 characters."
        required: true
        schema:
          $ref: "#/definitions/SchoolCreateAndUpdateBody"
      responses:
        200:
          description: "Returns request's success and the info of the new created element.\nThe `message` and `code` properties appear only if `success` is false."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                $ref: "#/definitions/School"
  /schools/{_id}:
    get:
      tags:
        - schools
      summary: Get info of a school
      description: Returns the info of the school of the given id.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the school to get info"
        required: true
        type: "string"
      responses:
        200:
          description: "Returns request's success and the info of the given element.\nThe `message` and `code` properties appear only if `success` is false."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                $ref: "#/definitions/School"
    put:
      tags:
        - schools
      summary: Update a school
      description: Updates a school info in the database.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the school to update"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Object containing the infos needed to be updated in the database.\n Property `cnpj` must have at least 14 characters."
        required: true
        schema:
          $ref: "#/definitions/SchoolCreateAndUpdateBody"
      responses:
        200:
          description: "Returns request's success and the success of the update.\nThe `message` and `code` properties appear only if `success` is false.\nThe `result` is true if element was updated in the database."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                type: "boolean"
    delete:
      tags:
        - schools
      summary: Delete a school
      description: Deletes a school info of the database.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the school to delete"
        required: true
        type: "string"
      responses:
        200:
          description: "Returns request's success and the success of the deletion.\nThe `message` and `code` properties appear only if `success` is false.\nThe `result` is true if element was deleted of the database."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                type: "boolean"

  /groups:
    get:
      tags:
        - groups
      summary: Get all groups
      description: Returns a list containing all groups.
      responses:
        200:
          description: Returns request's success and a list of Groups info.
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              result:
                type: "array"
                items:
                  $ref: "#/definitions/Group"
    
    post:
      tags:
        - groups
      summary: Insert/create new group
      description: Inserts a new group in the database.
      parameters:
      - in: "body"
        name: "body"
        description: "Object containing the infos needed to be added in the database."
        required: true
        schema:
          $ref: "#/definitions/GroupCreateAndUpdateBody"
      responses:
        200:
          description: "Returns request's success and the info of the new created element.\nThe `message` and `code` properties appear only if `success` is false."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                $ref: "#/definitions/Group"
  /groups/{_id}:
    get:
      tags:
        - groups
      summary: Get info of a group
      description: Returns the info of the group of the given id.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the group to get info"
        required: true
        type: "string"
      responses:
        200:
          description: "Returns request's success and the info of the given element.\nThe `message` and `code` properties appear only if `success` is false."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                $ref: "#/definitions/Group"
    put:
      tags:
        - groups
      summary: Update a group
      description: Updates a group info in the database.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the group to update"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Object containing the infos needed to be updated in the database."
        required: true
        schema:
          $ref: "#/definitions/GroupCreateAndUpdateBody"
      responses:
        200:
          description: "Returns request's success and the success of update.\nThe `message` and `code` properties appear only if `success` is false.\nThe `result` is true if element was updated in the database."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                type: "boolean"
    delete:
      tags:
        - groups
      summary: Delete a group
      description: Deletes a group info of the database.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the group to delete"
        required: true
        type: "string"
      responses:
        200:
          description: "Returns request's success and the success of deletion.\nThe `message` and `code` properties appear only if `success` is false.\nThe `result` is true if element was deleted of the database."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                type: "boolean"

  /students:
    get:
      tags:
        - students
      summary: Get all students
      description: Returns a list containing all students.
      responses:
        200:
          description: Returns request's success and a list of Students info.
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              result:
                type: "array"
                items:
                  $ref: "#/definitions/Student"
    
    post:
      tags:
        - students
      summary: Insert/create new student
      description: Inserts a new student in the database.
      parameters:
      - in: "body"
        name: "body"
        description: "Object containing the infos needed to be added in the database."
        required: true
        schema:
          $ref: "#/definitions/StudentCreateAndUpdateBody"
      responses:
        200:
          description: "Returns request's success and the info of the new created element.\nThe `message` and `code` properties appear only if `success` is false."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                $ref: "#/definitions/Student"
  /students/{_id}:
    get:
      tags:
        - students
      summary: Get info of a student
      description: Returns the info of the student of the given id.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the student to get info"
        required: true
        type: "string"
      responses:
        200:
          description: "Returns request's success and the info of the given element.\nThe `message` and `code` properties appear only if `success` is false."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                $ref: "#/definitions/Student"
    put:
      tags:
        - students
      summary: Update a student
      description: Updates a student info in the database.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the student to update"
        required: true
        type: "string"
      - in: "body"
        name: "body"
        description: "Object containing the infos needed to be updated in the database."
        required: true
        schema:
          $ref: "#/definitions/StudentCreateAndUpdateBody"
      responses:
        200:
          description: "Returns request's success and the success of update.\nThe `message` and `code` properties appear only if `success` is false.\nThe `result` is true if element was updated in the database."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                type: "boolean"
    delete:
      tags:
        - students
      summary: Delete a student
      description: Deletes a student info of the database.
      parameters:
      - name: "_id"
        in: "path"
        description: "The id of the student to delete"
        required: true
        type: "string"
      responses:
        200:
          description: "Returns request's success and the success of deletion.\nThe `message` and `code` properties appear only if `success` is false.\nThe `result` is true if element was deleted of the database."
          schema:
            type: "object"
            properties:
              success:
                type: "boolean"
              code:
                type: "integer"
              message:
                type: "string"
              result:
                type: "boolean"